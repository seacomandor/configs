#!/usr/bin/env python3.5
# requires python-mpd2 (pip install python-mpd2)

from mpd import MPDClient


def pp():
    client = MPDClient()
    client.connect('localhost', 6600)
    status = client.status()
    if(status['state'] == 'play'):
        client.pause()
    else:
        client.play()


pp()
